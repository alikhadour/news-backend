<?php
namespace App\Services;
use App\Interface\INewsService;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class NewsService implements INewsService
{
    public function fetchArticlesFromTheGuardian()
    {
        $url = "https://content.guardianapis.com/search?api-key=" . config('services.guardianapi.key');
        $response = Http::get($url);
        $articles = $response->json()['response']['results'];
        // return $articles;
        $articleModels = [];

        foreach ($articles as $articleData) {
            $publishedAt = Carbon::parse($articleData['webPublicationDate'])->toDateTimeString();

            $articleModel = [
                'source_id' => $articleData['id'],
                'source_name' => 'Guardian',
                'author' => null,
                'title' => $articleData['webTitle'],
                'description' => null,
                'url' => $articleData['webUrl'],
                'url_to_image' => null,
                'published_at' => $publishedAt,
                'content' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ];
            $articleModels[] = $articleModel;
        }

        Article::insert($articleModels);

        return $articleModels;
    }

    public function fetchArticlesFromNewsAPI()
    {
        $response = Http::get("https://newsapi.org/v2/top-headlines?language=en&apiKey=" . config('services.newsapi.key'));
        $articles = $response->json()['articles'];
        $articleModels = [];

        foreach ($articles as $articleData) {
            $publishedAt = Carbon::parse($articleData['publishedAt'])->toDateTimeString();

            $articleModel = [
                'source_id' => $articleData['source']['id'],
                'source_name' => $articleData['source']['name'],
                'author' => $articleData['author'],
                'title' => $articleData['title'],
                'description' => $articleData['description'],
                'url' => $articleData['url'],
                'url_to_image' => $articleData['urlToImage'],
                'published_at' => $publishedAt,
                'content' => $articleData['content'],
                'created_at' => now(),
                // Adjust as needed
                'updated_at' => now(), // Adjust as needed
            ];

            $articleModels[] = $articleModel;
        }

        // Store articles in your database
        Article::insert($articleModels);

        return $articleModels;
    }

    public function fetchArticles()
    {
        Article::truncate();
        return response()->json([
            'news-api' => $this->fetchArticlesFromNewsAPI(),
            'guardian-api' => $this->fetchArticlesFromTheGuardian(),
            'ny-times-api' => $this->fetchArticlesFromNYT()
        ]);
    }

    public function fetchArticlesFromNYT()
    {

        $response = Http::get("https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=" . config('services.nytimesapi.key'));
        // return  config('services.nytimesapi.key');
        $articles = $response->json()['response']['docs'];
        //return $articles;
        $articleModels = [];

        foreach ($articles as $articleData) {

            $publishedAt = Carbon::parse($articleData['pub_date'])->toDateTimeString();

            $articleModel = [
                'source_id' => $articleData['_id'],
                'source_name' => $articleData['source'],
                'author' => $articleData['byline']['original'],
                'title' => $articleData['headline']['main'],
                'description' => $articleData['snippet'],
                'url' => $articleData['web_url'],
                'url_to_image' => $articleData['multimedia'][0]['url'] ?? '',
                'published_at' => $publishedAt,
                'content' => $articleData['lead_paragraph'],
                // Content not provided in the data
                'created_at' => now(),
                'updated_at' => now(),
            ];

            $articleModels[] = $articleModel;
        }

        Article::insert($articleModels);

        return $articleModels;
    }
}
