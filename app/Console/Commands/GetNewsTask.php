<?php

namespace App\Console\Commands;

use App\Interface\INewsService;
use Illuminate\Console\Command;

class GetNewsTask extends Command
{

    
    private $newsService;

    public function __construct(INewsService $newsService)
    {
        parent::__construct();
        $this->newsService = $newsService;
    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-news-task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets All News From deferent sources and store them in the database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->newsService->fetchArticles();
        $this->info('Getting news is executed successfully.');

    }
}
