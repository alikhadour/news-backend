<?php 

namespace App\Interface;

interface INewsService
{
    public function fetchArticlesFromTheGuardian();
    public function fetchArticlesFromNewsAPI();

    public function fetchArticlesFromNYT();
    public function fetchArticles();

}
