<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:sanctum');
    }

    public function filterArticles(Request $request)
    {
        $date_filter = Carbon::parse($request['date'])->toDateTimeString();
        //return $date_filter;
        $articles = Article::where(function ($query) use ($request, $date_filter) {
            if ($request['source'] != null)
                $query->whereRaw('LOWER(source_name) like ?', ['%' . $request['source'] . '%']);
            if ($request['date'] != null)
                $query->whereDate('published_at', $date_filter);
            if ($request['keywords'])
                foreach ($request['keywords'] as $keyword) {
                    $query->where('content', 'LIKE', '%' . $keyword . '%');
                }
            
        })->get();

        return response()->json($articles);
    }
}